# # Exemption reissue notification Lambda Changelog

## v0.0.0 - 2 Jun 2023

:new: **New features**

- {feature}

:wrench: **Fixes**

- {issue}
