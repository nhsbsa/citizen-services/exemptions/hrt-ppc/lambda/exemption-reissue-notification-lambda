/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
export default {
  clearMocks: true,
  collectCoverageFrom: [
    "src/**/*.ts",
    "!src/**/index.ts",
    "!**/test/**",
    "!**/node_modules/**",
  ],
  coveragePathIgnorePatterns: ["/node_modules/"],
  coverageReporters: ["html", "text", "text-summary", "cobertura"],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 75,
      lines: 80,
      statements: 80,
    },
  },
  moduleDirectories: ["node_modules"],
  preset: "ts-jest",
  reporters: ["default"],
  setupFiles: ["<rootDir>/test-setup/setup.ts"],
  testEnvironment: "node",
  testMatch: ["**/*.test.ts"],
  testPathIgnorePatterns: ["<rootDir>/node_modules/"],
  verbose: true,
};
