import {
  CertificateType,
  NotificationMethod,
  NotificationPostResponse,
  NotificationPurpose,
  NotificationStatus,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { Channel } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";

export function mockNotificationRecord(
  method?: NotificationMethod,
): NotificationPostResponse {
  return {
    id: "notifapi-idxx-xxxx-xxxx-xxxxxxxxxxxx",
    certificateId: "certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
    certificateType: CertificateType.HRT_PPC,
    purpose: NotificationPurpose.REISSUE,
    method: method ?? NotificationMethod.EMAIL,
    status: NotificationStatus.PENDING,
    _meta: {
      channel: Channel.STAFF,
      createdTimestamp: new Date("2023-06-01T14:31:26.423507"),
      createdBy: "ABCD",
      updatedTimestamp: new Date("2023-06-01T14:31:26.423507"),
      updatedBy: "ABCD",
    },
    _links: {
      self: {
        href: "http://localhost:8130/v1/notifications/notifapi-idxx-xxxx-xxxx-xxxxxxxxxxxx",
      },
      notification: {
        href: "http://localhost:8130/v1/notifications/notifapi-idxx-xxxx-xxxx-xxxxxxxxxxxx",
      },
    },
  };
}

export function buildExpectedNotificationCall(method?: string) {
  return `
    [
      "certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
      {
        "channel": "STAFF",
        "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
        "user-id": "ABCD",
      },
      {
        "certificateType": "HRT_PPC",
        "method": \"${method ?? "EMAIL"}"\,
        "purpose": "REISSUE",
      },
    ]
    `;
}

export function buildExpectedUserPreferenceCall(channel?: string) {
  return `
      {
        "citizenId": "citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
        "headers": {
          "channel": \"${channel ?? "STAFF"}"\,
          "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
          "user-id": "ABCD",
        },
      }
    `;
}
