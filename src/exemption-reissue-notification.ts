import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import {
  InternalError,
  NotificationMethod,
  NotificationPostRequestBody,
  NotificationPurpose,
  UserPreferenceGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { findUserPrefByCitizenId, sendNotificationPost } from "./api-utils";
import {
  Channel,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";

const logger = loggerWithContext();

export async function reissueNotification({ headers, citizenId, certificate }) {
  logger.info("Entered reissue notification service");

  const {
    id: userPreferenceId,
    preference,
    _meta,
  }: UserPreferenceGetResponse = (await findUserPrefByCitizenId({
    citizenId,
    headers,
  })) || {};

  if (!userPreferenceId) {
    logger.error(
      `No user preference information found, failed to create notification record [citizenId: ${citizenId} certificateId: ${certificate.id}]`,
    );
    throw new InternalError("Internal Server Error", new Date());
  }

  const shouldSendNotification: boolean =
    [Channel.ONLINE, Channel.PHARMACY, Channel.STAFF].includes(_meta.channel) &&
    [Preference.EMAIL, Preference.POSTAL].includes(preference);

  if (shouldSendNotification) {
    const data: NotificationPostRequestBody = {
      purpose: NotificationPurpose.REISSUE,
      method:
        preference === Preference.POSTAL
          ? NotificationMethod.POST
          : NotificationMethod.EMAIL,
      certificateType: certificate.type,
    };
    const certificateId = certificate.id;
    await sendNotificationPost(certificateId, headers, data)
      .then(() => {
        logger.info(
          `Notification created successfully for [citizenId: ${citizenId} & certificateId: ${certificateId}]`,
        );
        return;
      })
      .catch((error) => {
        logger.error(
          `Notification creation unsuccessful for [citizenId: ${citizenId} & certificateId: ${certificateId}, error: ${error}]`,
        );
        throw new InternalError("Internal Server Error", new Date());
      });
  } else {
    logger.error(
      `User preference found, but is not the EMAIL, nor the POSTAL, [citizenId: ${citizenId} certificateId: ${certificate.id}, userPreferenceId: ${userPreferenceId}]`,
    );
    throw new InternalError("Internal Server Error", new Date());
  }
}
