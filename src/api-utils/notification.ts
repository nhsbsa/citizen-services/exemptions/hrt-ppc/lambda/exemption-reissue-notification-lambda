import {
  NotificationApi,
  NotificationPostRequestBody,
  NotificationPostResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const notificationApi = new NotificationApi();

/**
 * Responsible for making the POST request to the Notification API.
 *
 * @param certificateId
 * @param headers
 * @param data request body @see NotificationPostRequestBody
 * @returns response body @see NotificationPostResponse
 */
export async function sendNotificationPost(
  certificateId: string,
  headers,
  data: NotificationPostRequestBody,
): Promise<NotificationPostResponse> {
  return notificationApi.makeRequest({
    method: "POST",
    url: `/v1/notifications?certificateId=${certificateId}`,
    headers,
    data: data,
    responseType: "json",
  });
}
