import {
  CertificateType,
  UserPreferenceApi,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";

const userPreferenceApi = new UserPreferenceApi();

async function findUserPreferenceByCitizenIdAndCertificateType(
  citizenId,
  certificateType,
  headers,
) {
  loggerWithContext().info(
    `Calling userPreference api to find citizen notification preferences, citizenId:[${citizenId}], certificateType:[${certificateType}]`,
  );

  return userPreferenceApi.makeRequest({
    method: "GET",
    url: `/v1/user-preference/search/findByCitizenIdAndCertificateType?citizenId=${citizenId}&certificateType=${certificateType}`,
    headers,
    responseType: "json",
  });
}

export async function findUserPrefByCitizenId({ citizenId, headers }) {
  return findUserPreferenceByCitizenIdAndCertificateType(
    citizenId,
    CertificateType.HRT_PPC,
    headers,
  );
}
