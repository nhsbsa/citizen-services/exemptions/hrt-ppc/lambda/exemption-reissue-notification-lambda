import {
  CertificateType,
  NotificationApi,
  NotificationMethod,
  NotificationPostRequestBody,
  NotificationPurpose,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { sendNotificationPost } from "../notification";
import { mockNotificationRecord } from "../../__mocks__/mockDataFunctions";

describe("notification", () => {
  describe("sendNotificationPost()", () => {
    const certificateId = "certapix-idxx-xxxx-xxxx-xxxxxxxxxxxx";
    const headers = {
      header: "value",
    };

    it.each<NotificationMethod>([
      NotificationMethod.EMAIL,
      NotificationMethod.POST,
    ])(
      "should POST to notification API with valid request body",
      async (method: NotificationMethod) => {
        // given
        const requestBody: NotificationPostRequestBody = {
          purpose: NotificationPurpose.REISSUE,
          method: method,
          certificateType: CertificateType.HRT_PPC,
        };
        const notificationApiSpy = jest
          .spyOn(NotificationApi.prototype, "makeRequest")
          .mockResolvedValue(mockNotificationRecord(method));

        // when
        const response = await sendNotificationPost(
          certificateId,
          headers,
          requestBody,
        );

        // then
        expect(notificationApiSpy).toHaveBeenCalledTimes(1);
        expect(notificationApiSpy).toHaveBeenCalledWith({
          method: "POST",
          url: `/v1/notifications?certificateId=${certificateId}`,
          headers,
          data: requestBody,
          responseType: "json",
        });
        expect(notificationApiSpy.mock.calls).toMatchSnapshot();
        expect(response).toStrictEqual(mockNotificationRecord(method));
      },
    );
  });
});
