import { UserPreferenceApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockUserPrefResponse } from "../../__mocks__";
import { findUserPrefByCitizenId } from "../user-preference";

let userPreferenceApiSpy: jest.SpyInstance;
const citizenId = "36ec409f-b700-4d17-aab5-eb3ebfbb0f87";
const headers = {
  header: "value",
};

beforeEach(() => {
  userPreferenceApiSpy = jest
    .spyOn(UserPreferenceApi.prototype, "makeRequest")
    .mockResolvedValue({});
});
afterEach(() => {
  jest.resetAllMocks();
});

describe("user-preference", () => {
  describe("findUserPrefByCitizenId()", () => {
    it("should return the response successfully", async () => {
      userPreferenceApiSpy.mockResolvedValue(mockUserPrefResponse);
      const response = await findUserPrefByCitizenId({ citizenId, headers });
      expect(response).toBe(mockUserPrefResponse);
    });

    it("should call the user preference api with correct parameters", async () => {
      await findUserPrefByCitizenId({ citizenId, headers });
      expect(userPreferenceApiSpy).toHaveBeenCalledTimes(1);
      expect(userPreferenceApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "headers": {
                "header": "value",
              },
              "method": "GET",
              "responseType": "json",
              "url": "/v1/user-preference/search/findByCitizenIdAndCertificateType?citizenId=${citizenId}&certificateType=HRT_PPC",
            },
          ],
        ]
      `);
    });
  });
});
