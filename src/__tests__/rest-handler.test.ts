import { handler } from "../index";
import { reissueNotification } from "../exemption-reissue-notification";
import mockEvent from "../../events/event.json";

jest.mock("../exemption-reissue-notification");

let event;
const mockReissueNotification = reissueNotification as jest.MockedFunction<
  typeof reissueNotification
>;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  event = JSON.parse(JSON.stringify(mockEvent));
  mockReissueNotification.mockClear();
  mockReissueNotification.mockResolvedValue();
});

describe("handler", () => {
  it("should respond with an internal error if req body is an empty object", async () => {
    event.body = JSON.stringify({});
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
  });

  it("should respond with an bad request error if request citizenId is empty", async () => {
    event.body = JSON.stringify({
      citizenId: "",
      certificate: {
        id: "36a71326-d841-4261-9c2e-c823f9892d7a",
        reference: "HRTAB12CD34",
        type: "HRT_PPC",
        startDate: "2022-09-14",
        endDate: "2023-10-13",
      },
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
  });

  it("should respond with an bad request error if request certificate field is empty", async () => {
    event.body = JSON.stringify({
      citizenId: "ac176001-8626-1bbe-8186-2769640e0012",
      certificate: {},
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
  });

  it("should respond with an internal server error", async () => {
    mockReissueNotification.mockRejectedValueOnce(
      Error("Internal Server Error"),
    );

    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
  });

  it("should respond with the 200 success response when notificate reissued successfully", async () => {
    const result = await handler(event);
    expect(result).toEqual({
      statusCode: 200,
      body: JSON.stringify(mockReissueNotification),
    });
  });

  it("should ignore the additional headers and pass mandatory headers to handler", async () => {
    event.headers = {
      ...event.headers,
      additionalHeader: "ignored",
    };
    await handler(event);
    expect(mockReissueNotification).toHaveBeenCalledTimes(1);
    expect(mockReissueNotification.mock.calls[0][0].headers)
      .toMatchInlineSnapshot(`
      {
        "channel": "STAFF",
        "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
        "user-id": "ABCD",
      }
    `);
  });
});
