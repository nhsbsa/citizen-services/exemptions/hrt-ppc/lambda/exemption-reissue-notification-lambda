import {
  InternalError,
  NotificationMethod,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { headers as mockHeaders } from "../../events/event.json";
import { mockCertificate, mockUserPrefResponse } from "../__mocks__";
import { reissueNotification } from "../exemption-reissue-notification";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";
import * as userPreference from "../api-utils/user-preference";
import * as notification from "../api-utils/notification";
import {
  buildExpectedNotificationCall,
  buildExpectedUserPreferenceCall,
  mockNotificationRecord,
} from "../__mocks__/mockDataFunctions";
import {
  Channel,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";

let findUserPreferenceForCitizenSpy: jest.SpyInstance;
let sendNotificationPostSpy: jest.SpyInstance;

let mockCitizenId;
let userPreferenceRecord;
let notificationRecord;
let winstonInfoLoggerSpy;
let winstonErrorLoggerSpy;

beforeEach(() => {
  userPreferenceRecord = { ...mockUserPrefResponse };
  notificationRecord = { ...mockNotificationRecord() };

  findUserPreferenceForCitizenSpy = jest
    .spyOn(userPreference, "findUserPrefByCitizenId")
    .mockResolvedValue(userPreferenceRecord);

  sendNotificationPostSpy = jest
    .spyOn(notification, "sendNotificationPost")
    .mockResolvedValue(notificationRecord);

  winstonInfoLoggerSpy = jest.spyOn(logger, "info");
  winstonErrorLoggerSpy = jest.spyOn(logger, "error");
  jest.useFakeTimers().setSystemTime(new Date(Date.now()));
  mockCitizenId = "citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx";
});

afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
  jest.resetModules();
});

describe("reissueNotification()", () => {
  describe("without correct user preference", () => {
    afterEach(() => {
      // then
      expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
      expect(sendNotificationPostSpy).toBeCalledTimes(0);
      expect(sendNotificationPostSpy).not.toBeCalled();
      expect(winstonInfoLoggerSpy).toBeCalledTimes(1);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        1,
        "Entered reissue notification service",
      );
    });
    it("should throw an internal server error when findUserPrefByCitizenId has no response", async () => {
      // given
      findUserPreferenceForCitizenSpy.mockResolvedValue(null);

      // when / then
      await expect(
        reissueNotification({
          headers: mockHeaders,
          citizenId: mockCitizenId,
          certificate: mockCertificate,
        }),
      ).rejects.toThrow(new InternalError("Internal Server Error", new Date()));
      expect(winstonErrorLoggerSpy).toBeCalledTimes(1);
      expect(winstonErrorLoggerSpy).toHaveBeenNthCalledWith(
        1,
        "No user preference information found, failed to create notification record [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });

    it("should throw an internal server error when findUserPrefByCitizenId has an error response", async () => {
      findUserPreferenceForCitizenSpy.mockRejectedValueOnce(
        new Error("user pref api error"),
      );

      await expect(
        reissueNotification({
          headers: mockHeaders,
          citizenId: mockCitizenId,
          certificate: mockCertificate,
        }),
      ).rejects.toThrow(new InternalError("user pref api error", new Date()));
      expect(winstonErrorLoggerSpy).toBeCalledTimes(0);
    });

    it("should throw an internal server error when findUserPrefByCitizenId has an unknown preference", async () => {
      findUserPreferenceForCitizenSpy.mockResolvedValue({
        ...mockUserPrefResponse,
        preference: "INVALID_PREF",
      });

      await expect(
        reissueNotification({
          headers: mockHeaders,
          citizenId: mockCitizenId,
          certificate: mockCertificate,
        }),
      ).rejects.toThrow(new InternalError("Internal Server Error", new Date()));
      expect(winstonErrorLoggerSpy).toBeCalledTimes(1);
      expect(winstonErrorLoggerSpy).toHaveBeenNthCalledWith(
        1,
        "User preference found, but is not the EMAIL, nor the POSTAL, [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, userPreferenceId: 64400001-87dd-1573-8187-e13541b10002]",
      );
    });
  });

  describe("when user preference is EMAIL", () => {
    it.each([Channel.ONLINE, Channel.PHARMACY, Channel.STAFF])(
      "should process without error and POST notification when EMAIL and channel %p",
      async (channel) => {
        // given
        userPreferenceRecord._meta.channel = channel;

        // when
        await expect(
          reissueNotification({
            headers: mockHeaders,
            citizenId: mockCitizenId,
            certificate: mockCertificate,
          }),
        ).resolves.not.toThrowError();

        // then
        expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
        expect(
          findUserPreferenceForCitizenSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(buildExpectedUserPreferenceCall());
        expect(sendNotificationPostSpy).toBeCalledTimes(1);
        expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
          buildExpectedNotificationCall(),
        );
        expect(winstonInfoLoggerSpy).toBeCalledTimes(2);
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          1,
          "Entered reissue notification service",
        );
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          2,
          "Notification created successfully for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
        );
        expect(winstonErrorLoggerSpy).toBeCalledTimes(0);
        expect(winstonErrorLoggerSpy).not.toBeCalled();
      },
    );

    it("should throw an internal server error when sendNotificationPost throws an error", async () => {
      //given
      sendNotificationPostSpy = jest
        .spyOn(notification, "sendNotificationPost")
        .mockRejectedValue(new Error("Internal server error"));

      // when
      await expect(
        reissueNotification({
          headers: mockHeaders,
          citizenId: mockCitizenId,
          certificate: mockCertificate,
        }),
      ).rejects.toThrow(new InternalError("Internal Server Error", new Date()));

      // then
      expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
      expect(
        findUserPreferenceForCitizenSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(buildExpectedUserPreferenceCall());
      expect(sendNotificationPostSpy).toBeCalledTimes(1);
      expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
        buildExpectedNotificationCall(),
      );
      expect(winstonErrorLoggerSpy).toBeCalledTimes(1);
      expect(winstonErrorLoggerSpy).toHaveBeenNthCalledWith(
        1,
        "Notification creation unsuccessful for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, error: Error: Internal server error]",
      );
    });
  });

  describe("when user preference is POSTAL", () => {
    it.each([Channel.ONLINE, Channel.PHARMACY, Channel.STAFF])(
      "should process without error and POST notification when POSTAL and channel %p",
      async (channel) => {
        // given
        userPreferenceRecord.preference = Preference.POSTAL;
        userPreferenceRecord._meta.channel = channel;

        // when
        await expect(
          reissueNotification({
            headers: mockHeaders,
            citizenId: mockCitizenId,
            certificate: mockCertificate,
          }),
        ).resolves.not.toThrowError();

        // then
        expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
        expect(
          findUserPreferenceForCitizenSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(buildExpectedUserPreferenceCall());
        expect(sendNotificationPostSpy).toBeCalledTimes(1);
        expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
          buildExpectedNotificationCall(NotificationMethod.POST),
        );
        expect(winstonInfoLoggerSpy).toBeCalledTimes(2);
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          1,
          "Entered reissue notification service",
        );
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          2,
          "Notification created successfully for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
        );
        expect(winstonErrorLoggerSpy).toBeCalledTimes(0);
        expect(winstonErrorLoggerSpy).not.toBeCalled();
      },
    );

    it("should throw an internal server error when sendNotificationPost throws an error", async () => {
      //given
      userPreferenceRecord.preference = Preference.POSTAL;
      sendNotificationPostSpy = jest
        .spyOn(notification, "sendNotificationPost")
        .mockRejectedValue(new Error("Internal server error"));

      // when
      await expect(
        reissueNotification({
          headers: mockHeaders,
          citizenId: mockCitizenId,
          certificate: mockCertificate,
        }),
      ).rejects.toThrow(new InternalError("Internal Server Error", new Date()));

      // then
      expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
      expect(
        findUserPreferenceForCitizenSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(buildExpectedUserPreferenceCall());
      expect(sendNotificationPostSpy).toBeCalledTimes(1);
      expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
        buildExpectedNotificationCall(NotificationMethod.POST),
      );
      expect(winstonErrorLoggerSpy).toBeCalledTimes(1);
      expect(winstonErrorLoggerSpy).toHaveBeenNthCalledWith(
        1,
        "Notification creation unsuccessful for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, error: Error: Internal server error]",
      );
    });
  });

  it("should throw internal server error when 500 error code returned by notification api", async () => {
    // given
    sendNotificationPostSpy = jest
      .spyOn(notification, "sendNotificationPost")
      .mockRejectedValueOnce({
        status: 500,
      });

    // when
    await expect(
      reissueNotification({
        headers: mockHeaders,
        citizenId: mockCitizenId,
        certificate: mockCertificate,
      }),
    ).rejects.toThrow(new InternalError("Internal Server Error", new Date()));

    // then
    expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
    expect(
      findUserPreferenceForCitizenSpy.mock.calls[0][0],
    ).toMatchInlineSnapshot(buildExpectedUserPreferenceCall());
    expect(sendNotificationPostSpy).toBeCalledTimes(1);
    expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
      buildExpectedNotificationCall(),
    );
    expect(winstonErrorLoggerSpy).toBeCalledTimes(1);
    expect(winstonErrorLoggerSpy).toHaveBeenNthCalledWith(
      1,
      "Notification creation unsuccessful for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, error: [object Object]]",
    );
    expect(winstonInfoLoggerSpy).toBeCalledTimes(1);
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      1,
      "Entered reissue notification service",
    );
  });
});
