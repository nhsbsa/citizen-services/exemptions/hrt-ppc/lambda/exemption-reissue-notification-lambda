import { APIGatewayEvent } from "aws-lambda";
import {
  loggerWithContext,
  getHeaderValue,
  setCorrelationId,
} from "@nhsbsa/hrt-ppc-npm-logging";
import {
  errorResponse,
  InternalError,
  transformHeadersAndReturnMandatory,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { reissueNotification } from "./exemption-reissue-notification";

const SUCCESS_RESPONSE = {
  statusCode: 200,
};
const logger = loggerWithContext();

const logRequest = (headers, requestId) => {
  const correlationId = getHeaderValue(headers, "correlation-id");
  const channel = getHeaderValue(headers, "channel");
  const userId = getHeaderValue(headers, "user-id");

  logger.info(
    `Request received [requestId: ${requestId}][correlation-id: ${correlationId}][user-id: ${userId}][channel: ${channel}]`,
  );
  if (!correlationId || !channel || !userId) {
    logger.error(
      "Invalid request, headers must contains correlation-id, user-id and channel",
    );
    throw new InternalError("Internal Server Error", new Date());
  }
  logger.info("Request headers validated");
};

export async function handler(event: APIGatewayEvent) {
  try {
    logger.info("exemption-reissue-notification-lambda");
    const {
      headers,
      body,
      requestContext: { requestId },
    } = event;

    setCorrelationId(getHeaderValue(headers, "correlation-id"));

    const mandatoryHeaders = transformHeadersAndReturnMandatory(headers);

    logRequest(mandatoryHeaders, requestId);

    const payloadBody = JSON.parse(body || "{}");

    const { citizenId, certificate } = payloadBody;
    if (
      !citizenId ||
      !certificate ||
      !certificate.id ||
      !certificate.type ||
      !certificate.reference ||
      !certificate.startDate ||
      !certificate.endDate
    ) {
      logger.error(
        "Invalid request, body must contains citizenId, certificate.id, certificate.type, certificate.reference, certificate.startDate, and certificate.endDate",
      );
      throw new InternalError("Internal Server Error", new Date());
    }
    logger.info("Request body validated");

    await reissueNotification({
      headers: mandatoryHeaders,
      citizenId,
      certificate,
    });
    return SUCCESS_RESPONSE;
  } catch (err) {
    logger.error({ message: err });
    return errorResponse(err);
  }
}
