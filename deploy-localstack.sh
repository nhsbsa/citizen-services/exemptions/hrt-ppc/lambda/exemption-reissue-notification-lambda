#!/bin/bash

npm run build
echo "copying node_modules to /dist folder"
cp -R node_modules dist
echo "zipping /dist folder"
zip -9 -r -q exemption-reissue-notification-lambda.zip dist
echo "copying zip to /hrt-ppc-localstack folder"
cp exemption-reissue-notification-lambda.zip ../hrt-ppc-localstack/lambda-archives
echo "removing zip"
rm -f exemption-reissue-notification-lambda.zip
echo "removing node_modules from /dist folder"
rm -rf dist/node_modules